this repository will contains scripts for all sorts of genome annotation methods and/or parsing script.

###using git lab (the basics):
1. before you do anything you want to make sure the web copy and our local copy are the same things (i.e. they are both updated to the latest version): Type the following command:
   git pull
   This pulls all the changes that might exist on the web server for our repository.
2. Now copy the file/script in to this repository (called GenomeAnnotations)
3. Now propose the change to this repository (this means that the change is indexed or staged):
   git add <filename>
4. To commit these change to the head:
   git commit -m "add <filename>"
5. We have these changes in our local repository, but they are not on web copy yet. To push these changes to the web, type the following command:
   git push -u origin master

*Check whether this worked by going to the web link.

