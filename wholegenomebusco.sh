#!/bin/bash
#SBATCH --job-name=busco
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=32
#SBATCH --partition=general
#SBATCH --mail-type=END
####SBATCH --mem=10G
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH -o busco_%j.out
#SBATCH -e busco_%j.err

module load busco/3.0.2b
module unload augustus
export PATH=/home/CAM/szaman/augustus-3.2.3/bin:/home/CAM/szaman/augustus-3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus-3.2.3/config

run_BUSCO.py -i /UCHC/LABS/Wegrzyn/ConiferGenomes/Pita/pita.v2.0.1.masked3k2/genomes/pita.v2.0.1.NewMasked.5k.fasta -l /isg/shared/databases/busco_lineages/embryophyta_odb9/ -o Busco_PITA_wholeGenome -m geno -c 32
