import linecache
import argparse
import os

parser = argparse.ArgumentParser(
     prog='FilterLen.py',
     usage='''python parseBrakerGFF3.py --fasta [fasta file given by braker] --path [Path of fasta file] --gff [name of gff3 file] --pathGFF [path of the gff3 file] --partials [whether user wants both partial and complete genes] --newGFF [create new filtered gff] --monoexonics [retain monoexonic genes or not]''',
     description='''This program parse braker output and produces fasta files that contain correct complete and partial multi and monoexonic genes''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--fasta', type=str, help='The name of the fasta file', required=True)
parser.add_argument('--path', type=str, help='The path of the fasta file', required=False)
parser.add_argument('--gff', type=str, help='name of the gff file', required=True)
parser.add_argument('--pathGFF', type=str, help='path of gff file', required=False)
parser.add_argument('--partials', type=str, help='whether both completes and partials are wanted, if set to false, only complete genes are reported', required=True)
parser.add_argument('--newGFF', type=str, help='creates a new gff based on filtering', required=True)
parser.add_argument('--monoexonics', type=str, help='whether monoexonics gene should be retained or not, if set to false, only multiexonic genes are reported', required=True)

args=parser.parse_args()
fastapath=args.path
fasta=args.fasta
gff=args.gff
pathGFF=args.pathGFF
geneType=args.partials
create=args.newGFF
keepMonoBool=args.monoexonics

if fastapath==None:
    fastafile=fasta
else:
    fastafile=os.path.join(fastapath, fasta)

if pathGFF==None:
    gfffile=gff
else:
    gfffile=os.path.join(pathGFF, gff)
    
mrna=[]
mrnalines=[]
i=1
for row in open(gfffile,'r'):
    if "\tmRNA\t" in row:
        mrna.append(row[1:])
        mrnalines.append(i)
    i+=1    
mrnalines.append(i)

print "number of transcripts predicted:", len(mrna)

completeMono=[]
completeMulti=[]
partialMulti=[]
partialMono=[]
internalMulti=[]
internalMono=[]

correctCompMulti=0
correctPartMulti=0
keepMulti=[]
keepMono=[]
cm=0
cmo=0
pm=0
pmo=0
im=0
imo=0
acc=0
rej=0
gffNew=[]
for transcript in mrna:
    k=mrna.index(transcript)
    entry1=mrnalines[k]
    entry2=mrnalines[k+1]
    block=""
    cols=transcript.split("\t")
    name=cols[8].split(";")
    for m in range(entry1,entry2-1):
        block+=linecache.getline(gfffile,m)
    if "stop_codon" in block and "start_codon" in block and "single" in block:
        cmo=cmo+1
        if keepMonoBool=="True":
            keepMono.append(name[0][3:])
            gffNew.append(block)
    elif "stop_codon" in block and "start_codon" in block and "single" not in block:
        cm=cm+1
        if block.count("\texon\t")==block.count("\tintron\t")+1:
            correctCompMulti=correctCompMulti+1
            keepMulti.append(name[0][3:])
            gffNew.append(block)
        else:
            print block
    elif ("stop_codon" in block and "start_codon" not in block) or ("stop_codon" not in block and "start_codon" in block) and "single" in block:	
	pmo=pmo+1
        if geneType=="True" and keepMonoBool=="True":
            keepMono.append(name[0][3:])
            gffNew.append(block)
    elif ("stop_codon" in block and "start_codon" not in block) or ("stop_codon" not in block and "start_codon" in block) and "single" not in block:
     	pm=pm+1
        if block.count("\texon\t")==block.count("\tintron\t")+1: 
            correctPartMulti=correctPartMulti+1	
            if geneType=="True":
                keepMulti.append(name[0][3:])
                gffNew.append(block)
        else:
            rows=block.split("\n")
            if "\tintron\t" in rows[1] and "start_codon" in block and "\t-\t" in block:
                acc=acc+1
                if geneType=="True":
                    keepMulti.append(name[0][3:])
                    gffNew.append(block)
            elif "\tintron\t" in rows[len(rows)-2] and "start_codon" in block and "\t+\t" in block:
                acc=acc+1
                if geneType=="True":
                    keepMulti.append(name[0][3:])
                    gffNew.append(block)
            else:
                rej=rej+1
                print block
    elif "stop_codon" not in block and "start_codon" not in block and "single" in block:	
	imo=imo+1	
    elif "stop_codon" not in block and "start_codon" not in block and "single" not in block:
     	im=im+1

print "correct  complete/partial multiexonic gene is defined to be when: number of exons=number of introns + 1."
print "number of complete multi: ", cm
print "number of complete mono: ", cmo
print "number of partial multi: ", pm
print "number of partial mono: ",  pmo
print "number of internal multi: ",im
print "number of internal mono: ", imo
print "number of correct complete multi: ", correctCompMulti
print "number of correct partial multi: ", correctPartMulti
print "number of incorrect partial multiexonic genes that were accepted: ", acc
print "number of incorrect partial multiexonic genes that were rejected: ", rej
print "total number of partial multiexonic genes accepted:", correctPartMulti+acc

fastanames=[]
fastalines=[]
i=1
for row in open(fastafile,'r'):
    if row.startswith(">") == True:
        parts=row.split(".")
        fastanames.append(parts[1]+"."+parts[2].strip("\n"))
        fastalines.append(i)
    i+=1    
fastalines.append(i)

print fastanames[:5]
print keepMulti[:5]
print keepMono[:5]
print "number of multiexonic genes being kept", len(keepMulti)
print "number of monoexonic genes being kept", len(keepMono)

with open("MultiexonicGenes.fasta",'w') as multi, open("MonoexonicGenes.fasta",'w') as mono:
   for name in fastanames:
       if name in keepMulti or name in keepMono:
           k=fastanames.index(name)
           entry1=fastalines[k]
           entry2=fastalines[k+1]
           dnaseq=""
           for m in range(entry1+1,entry2):
               dnaseq+=linecache.getline(fastafile,m)
           if name in keepMulti:
               multi.write("%s%s%s%s%s" %(">",name,"\n",dnaseq,"\n"))
           else:
               mono.write("%s%s%s%s%s" %(">",name,"\n",dnaseq,"\n"))

if create=="True":
    with open("augustus.hints.filtered.gff3", 'w') as xyz:
        for g in gffNew:
            xyz.write(g)
xyz.close()
