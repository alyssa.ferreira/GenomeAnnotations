#!/bin/bash
# Submission script for Xanadu
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
####SBATCH --mem=350GB
#SBATCH --job-name=gth
#SBATCH -o repeatmasker-%j.output
#SBATCH -e repeatmasker-%j.error
#SBATCH --mail-user=sumaira.zaman@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --partition=general
#SBATCH --array=1-499%20

# Run the program                 
echo pita.v2.0.1.masked.5k.fasta"$SLURM_ARRAY_TASK_ID".fa

module load RepeatMasker/4.0.6

RepeatMasker pita.v2.0.1.masked.5k.fasta"$SLURM_ARRAY_TASK_ID".fa -dir /tempdata3/sumaira/RepeatMasker/Loblolly/ -lib /tempdata3/sumaira/RepeatMasker/Loblolly/PitaRepeats.fasta -pa 4 -gff -a -noisy -low -xsmall| tee output_pita.v2.0.1.masked.5k.fasta"$SLURM_ARRAY_TASK_ID".fa.txt
