#!/bin/bash
# Submission script for Xanadu
####SBATCH --time=10-01:00:00 # days-hh:mm:ss
####SBATCH --mem=350GB
#SBATCH --job-name=EBW
#SBATCH -o repeatmodeler-%j.output
#SBATCH -e repeatmodeler-%j.error
#SBATCH --mail-user=alexander.trouern-trend@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=22
#SBATCH --partition=general

# Run the program                 
##### NOTE #####
# After running build database function, index files that have been created
# should be manually copied to your scratch directory

export DATADIR='/UCHC/LABS/Wegrzyn/WalnutGenomes/EasternBlack'
export SEQFILE="EasternBW.fa"
export DATABASE="EasternBW"


WORKDIR=/scratch/$USER/repeatmodeler/$JOB_ID
mkdir -p $WORKDIR
cp $DATADIR/$SEQFILE $WORKDIR
cd $WORKDIR

module load RepeatModeler/1.0.8
module load rmblastn/2.2.28

#BuildDatabase -name EasternBW -engine ncbi EBW_asm-3.scafSeq
nice -n 10 RepeatModeler -engine ncbi -pa 22 -database $DATABASE
rsync -a ./consensi.fa.classified $DATADIR/$SEQFILE.consensi.fa.classified

