import linecache
import argparse
import os

parser = argparse.ArgumentParser(
     prog='FilterAA.py',
     usage='''python FilterAA.py --fasta [fasta file] --path [Path of fasta file] --out [name of output file] ''',
     description='''this program removes proteins that do not begin with methionine and that are less than 150 amino acids''',
     epilog='''It requires linecache, argparse, and os''')
parser.add_argument('--fasta', type=str, help='The name of the fasta file', required=True)
parser.add_argument('--out', type=str, help='name of output file', required=True)
parser.add_argument('--path', type=str, help='path of fasta file', required=False)

args=parser.parse_args()
fastapath=args.path
fasta=args.fasta
partial="5prime_"+args.out+"150aa.faa"
complete="comp_"+args.out+"150aa.faa"

if fastapath==None:
    filename1=fasta
else:
    filename1=os.path.join(fastapath, fasta)

fastanames=[]
fastalines=[]
i=1
for row in open(filename1,'r'):
 if row.startswith(">") == True:
     fastanames.append(row[1:])
     fastalines.append(i)
 i+=1    
fastalines.append(i)

with open(partial,'w') as p, open(complete,'w') as c:
   for name in fastanames:
       k=fastanames.index(name)
       entry1=fastalines[k]
       entry2=fastalines[k+1]
       dnaseq=""
       for m in range(entry1+1,entry2):
          dnaseq+=linecache.getline(filename1,m)
	  cleanseq=dnaseq.strip("\n")    
       if len(dnaseq)>=150 and dnaseq[0]=="M" and cleanseq[-1]=="*":
	  c.write("%s%s%s" %(">",name,dnaseq))  
       elif len(dnaseq)>=150 and dnaseq[0]=="M":
	  p.write("%s%s%s" %(">",name,dnaseq))
p.close()
c.close()

