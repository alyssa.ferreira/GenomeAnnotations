#!usr/bin/perl
open (GFF, $ARGV[0])
        or die "No input GFF specified!!!";                    # This is the input file specified by gstats.pl
open (OUTPUT, $ARGV[1])
        or die "No out output destination specified!!!";        # This is the output destination specified by gstats.pl

@GFF = <GFF>;
$GFF = join "", @GFF;
@GFF = split "\n", $GFF;

#$temp_loc = "$ARGV[1]\/temp_file.txt";
$temp_loc = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "temp_file.txt";

open (TEMPORARY_OUTFILE, ">$temp_loc");
        foreach $line (@GFF){
                $line =~ s/\s+$//;
                @split_line = split "\t", $line;

	if ($line =~ /^\#/){
		next;
	}
	else{
                if ($split_line[2] =~ /gene/){
                        print TEMPORARY_OUTFILE "###", "\n",
                                $line, "\n";
                }
                else{print TEMPORARY_OUTFILE $line, "\n";}
	}
        }	
close TEMPORARY_OUTFILE;
close GFF;

#############################################################################################################################################
# Transcripts need to be resolved.

open (GFF, "$temp_loc") or die "Cannot find temp_file.txt at $ARGV[1]!!!";
@GFF = <GFF>;
$GFF = join "", @GFF;
@GFF = split "###", $GFF;

close GFF;
open (TEMPORARY_OUTFILE, ">$temp_loc");

foreach $gene (@GFF){
        $gene =~ s/\s+$//g;
        @lines = split "\n", $gene;

        foreach $line (@lines){
                @split_line = split "\t", $line;
                if ($split_line[2] =~ /gene/){
			@split_eight = split ";", $split_line[8];			
			$scaffold = $split_line[0];
			$start = $split_line[3];				
			$stop =  $split_line[4];
				if ($scaffold =~ $scaffold_pre and $start =~ $start_pre and  $stop =~ $stop_pre){		
					print TEMPORARY_OUTFILE $gene, "\n";
				}
				else{
					$transcript = $1;				
					print TEMPORARY_OUTFILE "###", $gene, "\n";
					$scaffold_pre = $scaffold;
					$start_pre =  $start;
					$stop_pre = $stop;
					$count = 1;
				}
		}
		
	}
}

close TEMPORARY_OUTFILE;

open (GFF, "$temp_loc") or die "Cannot find temp_file.txt at $ARGV[1]!!!";
@GFF = <GFF>;
$GFF = join "", @GFF;
@GFF = split "###", $GFF;

$gene_table = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "gene_table.txt";
open (GENE_TABLE, ">$gene_table") or die "Cannot find gene_table.txt at $ARGV[1]!!!";
print GENE_TABLE "###", "\n";


foreach $gene (@GFF){
        $gene =~ s/\s+$//g;
        @lines = split "\n", $gene;

        foreach $line (@lines){
                @split_line = split "\t", $line;
		
                if ($split_line[2] =~ /gene/){
			$scaffold = $split_line[0];
			$strand = $split_line[6];
			@split_colon = split ";", $split_line[8];
			 $split_colon[1] =~ s/\s+$//;
			$ID = $split_colon[1];
			if ($gene_line ==0){
				$gene_line++;
        	                $gene_length = $split_line[4] - $split_line[3] + 1;
			@split_colon = split ";", $split_line[8];
			$split_colon[1] =~ s/\s+$//;

                        print GENE_TABLE $split_line[2],                        # gene
                                "\t", $gene_length,                             # length
                                "\t", $split_line[3],                           # start
                                "\t", $split_line[4],                           # stop
                                "\t", $split_line[6],                           # strand
                                "\t", $split_colon[1],                           # ID
                                "\t", $split_line[0],
                                "\n";
			}
                }
                if ($split_line[2] =~ /exon/){
                        $exon_length = $split_line[4] - $split_line[3] + 1;
                         print GENE_TABLE $split_line[2],                       # exon
                                "\t", $exon_length,                             # length
                                "\t", $split_line[3],                           # start
                                "\t", $split_line[4],                           # stop
                                "\t", $split_line[6],                           # strand
                                "\t", $ID,		                           # ID
                                "\t", $split_line[0],
                                "\n";

                        push @exons, $split_line[3];
                        push @exons, $split_line[4];
                }


        }
	if (scalar @exons >= 4){
                @exons_sorted = sort {$a <=> $b} @exons;
                # 10    20      30      40      50      60
                #[0]    [1]-----[2]     [3]-----[4]     [5]
                # Intron = [2] - [1] and [4] - [3]

                for ($a = 2; $a < scalar @exons_sorted; $a = $a +2){
                      $end = $exons_sorted[$a] - 1;
                        $start = $exons_sorted[$a-1] + 1;
                        $intron_length = $end - $start + 1;

                                print GENE_TABLE "intron",
                                        "\t", $intron_length,
                                        "\t", $start,
                                        "\t", $end,
                                        "\t", $strand,
                                        "\t", ".",
                                        "\t", $scaffold,
                                        "\n";
                }
        }
        if(scalar @exons < 1){
                if ($gene =~ /.+/){
                        @split_line = split "\t", $gene;
                        $gene_length = $split_line[4] - $split_line[3] + 1;
                        print GENE_TABLE "exon",                                # gene
                                "\t", $gene_length,                             # length
                                "\t", $split_line[3],                           # start
                                "\t", $split_line[4],                           # stop
                                "\t", $strand,                           # strand
                                "\t", ".",
                                "\t", $scaffold,                           # ID
                                "\n";
                }
        }

print GENE_TABLE "###", "\n";
$gene_line = 0;
undef (@exons);
undef (@introns);
}
close GENE_TABLE;
close GFF;





