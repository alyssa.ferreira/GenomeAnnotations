#!usr/bin/perl
# Incomplete means there is "gene" space that is not accounted for by exons or introns/

$complete = 0;
$incomplete = 0;

open (GENE_TABLE, $ARGV[0]) or die "Cannot open the gene table!!!";
open (OUTPUT, $ARGV[1]);
	@infile = <GENE_TABLE>;
	$infile = join "", @infile;
	@infile = split "###", $infile;
close GENE_TABLE;

$comp = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "complete_genes.txt";
open (COMPLETE, ">$comp") or die "Cannot create complete_genes.txt at $ARGV[1]!!!";
print COMPLETE "###", "\n";

	foreach $gene (@infile){
		$gene =~ s/\s+$//g;
		@split_lines  = split "\n", $gene;
		
		foreach $line (@split_lines){
			@split_tab = split "\t", $line;

			if ($split_tab[0] =~ /gene/){
				#print $line, "\n";
				$start = "$split_tab[2]";
				$end = "$split_tab[3]";
			}

			if ($split_tab[0] =~ /exon/){
				if ($split_tab[4] =~ /\+/){
					if ($split_tab[2] == $start){
						$count++;
					}
				}
				if ($split_tab[4] =~ /\-/){
					if ($split_tab[3] == $end){
						$count++;
					}
				}
			}
		}

		#print $start, "\t", $end, "\n", $gene, "\n";
		if ($count == 1){
			if ($gene =~ /exon/){
				print COMPLETE $gene, "\n", "###";
				$complete++;
			}
		}
		else{
			if ($gene =~ /exon/){
				#print $gene, "\n", "###";
				$incomplete++;
			}
		}

		$count = 0;
		$start = 0;
		$end = 0;
}

print "Number of genes retained:\t", $complete, "\n";
print "Number of genes lost:\t", $incomplete, "\n";
