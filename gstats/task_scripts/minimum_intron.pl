#!usr/bin/perl
$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[3] . "gene_table.txt";

##ARGV[0] --> gene_table
##ARGV[1] --> Location
##ARGV[2] -->  Minimum size
##ARGV[3] --> Pre

open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;

$output = $ARGV[1] . "\/" . "\/" . $ARGV[3] . "minumum_introns_gene_table.txt";
open (OUTFILE, ">$output");
print OUTFILE "###", "\n";

GENE:foreach $gene (@gene_table){
	$count = 0;
	$gene =~ s/\s+$//g;
	@split_lines = split "\n", $gene;

	foreach $line (@split_lines){
		$line =~ s/\s+$//;

		@split_tab = split "\t", $line;

		if ($split_tab[0] =~ /intron/){
			if ($split_tab[1] < $ARGV[2]){
				$count++;		
			}
		}
	}
	if ($count == 0){
		print OUTFILE $gene, "\n", "###";
		$pass++;
	}
}

print "Number of genes retained:\t", $pass, "\n";
close OUTFILE;
