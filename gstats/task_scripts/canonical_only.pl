#!usr/bin/perl

# ARGV[0] --> in fasta
# ARGV[1] --> output location

use Bio::Index::Fasta; 

$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "canonical_genes.txt";
open (FASTA_1, ">$output") or die "Cannot create the output script!!!";
print FASTA_1 "###", "\n";

$input = $ARGV[0] . ".idx";
$inx = Bio::Index::Fasta->new(-filename => $input);

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";
open (GENE_TABLE, "$gene_table") or die "Cannot open $gene_table";

@gene_table = <GENE_TABLE>;
$gene_table = join "", @gene_table;
@gene_table = split "###", $gene_table;

close GENE_TABLE;
	GENE: foreach $gene (@gene_table){
		$gene =~ s/\s+$//g;
		@split_lines = split "\n", $gene;

		foreach $line (@split_lines){
			$line =~ s/\s+$//;
			@split_tab = split "\t", $line;

			if ($split_tab[0] =~ /intron/){
				$intron++;
				$split_start_2 = $split_tab[2] +1;
				$split_start_1 = $split_tab[2];
				$split_end_1 =  $split_tab[3] -1;
				$split_end_2 = $split_tab[3];

				$seq = $inx->fetch("$split_tab[6]");					

				$string_1 = $seq->subseq("$split_start_1", "$split_start_2");
				$string_1 =~ tr/ATGC/atgc/;
		
				$string_2 = $seq->subseq("$split_end_1", "$split_end_2");
				$string_2 =~ tr/ATGC/atgc/;

				$splice = "$string_1\_$string_2";

				if ($splice =~ /gt_ag/ or $splice =~ /ct_ac/){
					$canonical++;
				}
			}
		}	
	
	if ($intron == $canonical){
		print FASTA_1 $gene, "\n", "###";
		$saved++;
	}
	$canonical = 0;
	$intron = 0;
	}

close FASTA_1;

print "Genes retained:\t", $saved, "\n";
