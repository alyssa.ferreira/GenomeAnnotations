#!usr/bin/perl
$splice = 0;
open (GENE_TABLE, $ARGV[0]) or die "Cannot open gene table";
$overlap = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "overlapping_gene_table.txt";

open (OVERLAP, $overlap) or die "Cannot open $overlap";
	@overlap = <OVERLAP>;
	$infile = join "", @overlap;
	@infile = split "###", $infile;
close OVERLAP;

$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "transcripts_pre.txt";
open (OUTFILE, ">$output") or die "Cannot create $output";
print OUTFILE "###", "\n";

foreach $gene (@infile){
	$gene =~ s/\s+$//g;

	@split_lines = split "\n", $gene;

	LINE: for ($a = 0; $a < scalar @split_lines; $a++){		
		@split_current = split "\t", $split_lines[$a];
		@split_previous = split "\t", $split_lines[$a-1];
		@split_next = split "\t", $split_lines[$a+1];

		if ($split_current[0] =~ /exon/){
			if ($split_next[5] ne $split_current[5]){
				push @lines, $split_lines[$a];
				$count++;
				@split_tab = split "\t", $split_lines[1];
				print OUTFILE $split_tab[0], "\t",
						$split_tab[1], "\t",
						$split_tab[2], "\t",
						$split_tab[3], "\t",
						$split_tab[4], "\t",
						$split_tab[5], 
						 "\.", $count, "\t",
						$split_tab[6], "\n";
	
						$splice++;

					foreach $line (@lines){
						print OUTFILE $line, "\n";
					}
				print OUTFILE "###", "\n";
				undef (@lines);
				next LINE;
			}
			if ($split_next[5] =~ $split_current[5]){
                                push @lines, $split_lines[$a];

                        }
		}				
	}
	$count = 0;
}

close OUTFILE;

#REVALUATING INTRONS!!!
		
open (TRANSCRIPTS, "$output") or die "Cannot open $output";
	@TRANSCRIPTS = <TRANSCRIPTS>;
	$TRANSCRIPTS = join "", @TRANSCRIPTS;
	@transcripts = split "###", $TRANSCRIPTS;
close TRANSCRIPTS;

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "transcripts.txt";
open (GENE_TABLE, ">$gene_table") or die "Cannot create $gene_table";
print GENE_TABLE "###", "\n";

foreach $gene (@transcripts){
        $gene =~ s/\s+$//g;
        @lines = split "\n", $gene;

        foreach $line (@lines){
                @split_line = split "\t", $line;

                if ($split_line[0] =~ /gene/){
                        print GENE_TABLE $split_line[0],                        # gene
				"\t", $split_line[1],				# length
                                "\t", $split_line[2],                           # start
                                "\t", $split_line[3],                           # stop
                                "\t", $split_line[4],                           # strand
                                "\t", $split_line[5],                           # ID
				"\t", $split_line[6], 
                                "\n";
                }
                if ($split_line[0] =~ /exon/){
                         print GENE_TABLE $split_line[0],                       # exon
                                "\t", $split_line[1],                           # length
                                "\t", $split_line[2],                           # start
                                "\t", $split_line[3],                           # stop
                                "\t", $split_line[4],                           # strand
                                "\t", $split_line[5],                           # ID
				"\t", $split_line[6],
                                "\n";

                        push @exons, $split_line[2];
                        push @exons, $split_line[3];
                }


        }
 if (scalar @exons >= 4){
                @exons_sorted = sort {$a <=> $b} @exons;
                # 10    20      30      40      50      60
                #[0]    [1]-----[2]     [3]-----[4]     [5]
                # Intron = [2] - [1] and [4] - [3]

                for ($a = 2; $a < scalar @exons_sorted; $a = $a +2){
                        $intron_length = $exons_sorted[$a] - $exons_sorted[$a-1] + 1;
                       
			$start = $exons_sorted[$a - 1] +1;
			$stop = $exons_sorted[$a] -1;
			$length =  $stop - $start + 1;
                                print GENE_TABLE "intron",
                                        "\t", $length,
                                        "\t", $start,
                                        "\t", $stop,
                                        "\t", $split_line[4],
					"\t", ".",
					"\t", $split_line[6],
                                        "\n";
                       

                }
        }
        if(scalar @exons < 1){
                if ($gene =~ /.+/){
                        @split_line = split "\t", $gene;
                        $gene_length = $split_line[3] - $split_line[2] + 1;

                        print GENE_TABLE "exon",                                # gene
                                "\t", $gene_length,                             # length
                                "\t", $split_line[2],                           # start
                                "\t", $split_line[3],                           # stop
                                "\t", $split_line[4],                           # strand
                                "\t", $split_line[5],                           # ID
				"\t", $split_line[6],
                                "\n";
                }
        }
print GENE_TABLE "###", "\n";
undef (@exons);
undef (@introns);
}
close GENE_TABLE;

system "rm $output";

print "Number of overlapped genes resolved:\t", $splice, "\n";
