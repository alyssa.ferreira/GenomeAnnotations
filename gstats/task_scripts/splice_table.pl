#!usr/bin/perl

# ARGV[0] --> in fasta
# ARGV[1] --> output location

use Bio::Index::Fasta; 

$output = $ARGV[1] . "\/" . "\/" . $ARGV[2] . "splice_table.txt";
open (FASTA_1, ">$output") or die "Cannot create the output script!!!";

$input = $ARGV[0] . ".idx";
$inx = Bio::Index::Fasta->new(-filename => $input);

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";
open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";

@gene_table = <GENE_TABLE>;
$gene_table = join "", @gene_table;
@gene_table = split "###", $gene_table;

close GENE_TABLE;

	foreach $gene (@gene_table){
		@split_lines = split "\n", $gene;

		foreach $line (@split_lines){
			$line =~ s/\s+$//;
			@split_tab = split "\t", $line;

			if ($split_tab[0] =~ /intron/){
				$split_start_2 = $split_tab[2] +1;
				$split_start_1 = $split_tab[2];
				$split_end_1 =  $split_tab[3] -1;
				$split_end_2 = $split_tab[3];

				$seq = $inx->fetch("$split_tab[6]");					

				$string_1 = $seq->subseq("$split_start_1", "$split_start_2");
				$string_1 =~ tr/ATGC/atgc/;
		
				$string_2 = $seq->subseq("$split_end_1", "$split_end_2");
				$string_2 =~ tr/ATGC/atgc/;

				$splice = "$string_1\_$string_2";

				if ($split_tab[4] =~ /\-/){						#REVERSE STRAND SHENANIGANS
					$splice =~ tr/atgc/tacg/;
					@splice_split = split "", $splice;
					foreach $base (@splice_split){
						unshift @unshift, $base;
					}
					$splice = join "", @unshift;
					undef (@unshift);
				}
				 if ($splice =~ /tc_aa/){
                                        print FASTA_1 $gene, "\n";
                                }


				$Splice_lookup{$splice}++;

			}
		}	
	}


foreach $splice (keys %Splice_lookup){
	print FASTA_1 $splice, "\t", $Splice_lookup{$splice}, "\n";
}

close FASTA_1;
