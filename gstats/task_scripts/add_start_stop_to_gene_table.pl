#!usr/bin/perl
# ARGV[0] --> in fasta
# ARGV[1] --> output location
# ARGV[2] --> prefix

$start_end = 0;

use Bio::Index::Fasta;

$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "start_and_stop_gene_table.txt";
open (OUT_TABLE, ">$output") or die "Cannot create the output script!!!";
print OUT_TABLE "###", "\n";

$input = $ARGV[0] . ".idx";
$inx = Bio::Index::Fasta->new(-filename => $input);

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";
open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";

@gene_table = <GENE_TABLE>;
$gene_table = join "", @gene_table;
@gene_table = split "###", $gene_table;

close GENE_TABLE;

GENE: foreach $gene (@gene_table){
	$gene =~ s/\s+$//g;
        @split_lines = split "\n", $gene;

        foreach $line (@split_lines){
		$line =~ s/\s+$//;										#Print gene, exon, intron line
                        print OUT_TABLE $line, "\n";
                        @split_tab = split "\t", $line;

                        if ($split_tab[0] =~ /gene/){
				if ($split_tab[4] =~ /\+/){
					$start = $split_tab[2];								#1
					$start_end = $split_tab[2] +2;							#3
	                                $seq = $inx->fetch("$split_tab[6]");						#Gets scaffold sequence

        	                        $string_1 = $seq->subseq($start, $start_end);					#seq = 1-3
	                                $string_1 =~ tr/ATGC/atgc/;							#all lower case

					$end = $split_tab[3] -2;			
					$end_end = $split_tab[3];

					$length = $seq->length();
                                                
					if ($string_1 =~ /atg/){
						undef ($string_1);
							print OUT_TABLE "start_codon", "\t",
								"3", "\t",
								$start, "\t",
								$start_end, "\t",
								$split_tab[4], "\t",
								".", "\t",
								$split_tab[6], "\n";

					}
					if ($length > $end_end){							#Since stop codon are POST seq
                                              $string_end_1 = $seq->subseq($end, $end_end);
                                              $string_end_1 =~ tr/ATGC/atgc/;
					      if ($string_end_1 =~ /taa/ or $string_end_1 =~ /tga/ or $string_end_1 =~ /tag/){
							undef ($string_end_1);

							print OUT_TABLE "stop_codon", "\t",
                                                                "3", "\t",
                                                                $end, "\t",
                                                                $end_end, "\t",
                                                                $split_tab[4], "\t",
                                                                ".", "\t",
                                                                $split_tab[6], "\n";
						}
                                        }
				}
				if ($split_tab[4] =~ /\-/){
					$start_end =  $split_tab[3];					#In reverse due to strandedness
					$start = $split_tab[3] -2;
	
					$end = $split_tab[2];						
					$end_end = $split_tab[2] +2;

					$seq = $inx->fetch("$split_tab[6]");

					$string_1 = $seq->subseq($start, $start_end);
                                        $string_1 =~ tr/ATGC/atgc/;
					$string_1 =~ tr/atgc/tacg/;
					
					@split_neg = split "",  $string_1;
					$string_2 = $split_neg[2] . $split_neg[1] . $split_neg[0];		#inverting then rearranging 	

					if ($string_2 =~ /atg/){
                                                undef ($string_2);
						undef ($string_1);

						print OUT_TABLE "start_codon", "\t",
                                                                "3", "\t",
                                                                $start, "\t",
                                                                $start_end, "\t",
                                                                $split_tab[4], "\t",
                                                                ".", "\t",
                                                                $split_tab[6], "\n";
                                        }

					$length = $seq->length();

                                                if ($end >= 1){							#Rev strand stop is pre [2]	
							$string_end = $seq->subseq($end, $end_end);
							$string_end =~ tr/ATGC/atgc/;
                                		        $string_end =~ tr/atgc/tacg/;
							@split_neg_end = split "",  $string_end;
						
	                                        	@split_neg_end = split "",  $string_end;
							$string_2_end = $split_neg_end[2] . $split_neg_end[1] . $split_neg_end[0];				

						if ($string_2_end =~ /taa/ or $string_2_end =~ /tga/ or $string_2_end =~ /tag/){
							undef($string_end_1);
							undef($string_end);

							print OUT_TABLE "stop_codon", "\t",
                                                                "3", "\t",
                                                                $end, "\t",
                                                                $end_end, "\t",
                                                                $split_tab[4], "\t",
                                                                ".", "\t",
                                                                $split_tab[6], "\n";
        	                                }
					}
				}
			}
		}
		print OUT_TABLE "###";
	}

close OUT_TABLE;


