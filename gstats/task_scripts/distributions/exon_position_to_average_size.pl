#!usr/bin/perl

open (GENE_TABLE, $ARGV[0]) or die "Cannot open the gene table at $ARGV[1]!!!";
open (OUTPUT, $ARGV[1]);

$output = "$ARGV[1]distribution_exon_position_to_average_size.txt";
open (OUTFILE, ">$output");

print OUTFILE "Exon_position_in_gene\tAverage_size\tNumber_of_exons\n";

@infile = <GENE_TABLE>;
$infile = join "", @infile;
@infile = split "###", $infile;

close GENE_TABLE;

foreach $gene (@infile){
	
	if ($gene =~ /intron/){
	@split_line = split "\n", $gene;
	foreach $line (@split_line){
		$line =~ s/\s+$//;
		@split_tab = split "\t", $line;

		if ($split_tab[0] =~ /exon/){
			if ($split_tab[4] =~ /\+/){
				$positive++;
				push @$positive, $split_tab[1];
				push @exons, $positive;
			}
			
			if ($split_tab[4] =~ /\-/){
                                $negative++;
				unshift @temp, $split_tab[1];
				push @exons, $negative;
			}
		}
	}
$positive = 0;
$negative = 0;

	foreach $exon (@temp){
		$negative++;
		push @$negative, $exon;
	}

undef (@temp);
$negative = 0;
}
}

@sort = sort {$b <=> $a} @exons;

for ($a = 1; $a<= $sort[0]; $a++){
	
	foreach $exon (@$a){
		$lenghts_sum = $lenghts_sum + $exon;
		$count++;
	}

	$average = $lenghts_sum/$count;

	print OUTFILE $a, "\t", $average, "\t", scalar @$a, "\n";
	$lenghts_sum = 0;
	$count = 0;
}

	
close OUTFILE;
