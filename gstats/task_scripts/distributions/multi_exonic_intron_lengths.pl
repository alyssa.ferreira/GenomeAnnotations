#!usr/bin/perl

open (INFILE, $ARGV[0])  or die "Cannot open the gene table at $ARGV[1]!!!";
open (OUTPUT, $ARGV[1]);

$output = "$ARGV[1]distribution_multiexonic_intron_lengths.txt";
open (OUTFILE, ">$output");

print OUTFILE "Intron_size\tNumber_of_introns\n";

while ($line = <INFILE>){
	$line =~ s/\s+$//;
	@split = split "\t", $line;
	if ($split[0] =~ /intron/){
		push @introns, $split[1];
	}
}

close INFILE;
@sorted = sort {$a<=>$b} @introns;

for ($a = 0; $a < $sorted[scalar @introns - 1]; $a = $a + 100){
	$end = $a +100;
	$stop = $a +99;
	foreach $length (@sorted){
		if ($length >= $a and $length < $end){
	             $intron_occurances++;
	        }
	}
	print OUTFILE $a, "-", $stop, "\t", $intron_occurances, "\n";
	$intron_occurances = 0;
}

close OUTFILE;
