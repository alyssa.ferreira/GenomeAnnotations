#!usr/bin/perl

open (INFILE, $ARGV[0]) or die "Cannot open the gene table at $ARGV[1]!!!";
open (OUTPUT, $ARGV[1]);

$output = "$ARGV[1]distribution_multiexonic_gene_lengths.txt";
open (OUTFILE, ">$output");

print OUTFILE "Gene_size\tNumber_of_genes\n";

while ($line = <INFILE>){
	$line =~ s/\s+$//;
	@split = split "\t", $line;
	if ($split[0] =~ /gene/){
		push @genes, $split[1];
	}
}

close INFILE;
@sorted = sort {$a<=>$b} @genes;

for ($a = 0; $a < $sorted[scalar @genes - 1]; $a = $a + 1000){
	$end = $a +1000;
	$stop = $a +999;
	foreach $length (@sorted){
		if ($length >= $a and $length < $end){
	             $gene_occurances++;
	        }
	}
	print OUTFILE $a, "-", $stop, "\t", $gene_occurances, "\n";
	$gene_occurances = 0;
}

close OUTFILE;
