#!usr/bin/perl

open (INFILE, $ARGV[0]) or die "Cannot open the gene table at $ARGV[1]!!!";
open (OUTPUT, $ARGV[1]);

$output = "$ARGV[1]distribution_multiexonic_exon_lengths.txt";
open (OUTFILE, ">$output");

print OUTFILE "Exon_size\tNumber_of_exons\n";

while ($line = <INFILE>){
	$line =~ s/\s+$//;
	@split = split "\t", $line;
	if ($split[0] =~ /exon/){
		push @exons, $split[1];
	}
}

close INFILE;
@sorted = sort {$a<=>$b} @exons;

for ($a = 0; $a < $sorted[scalar @exons - 1]; $a = $a + 10){
	$end = $a +10;
	$stop = $a +9;
	foreach $length (@sorted){
		if ($length >= $a and $length < $end){
	             $exon_occurances++;
	        }
	}
	print OUTFILE $a, "-", $stop, "\t", $exon_occurances, "\n";
	$exon_occurances = 0;
}

close OUTFILE;
