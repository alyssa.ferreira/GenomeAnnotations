#!usr/bin/perl

#$ARGV[0] = start stop gene table
#$ARGV[1] = output location
#$ARGV[3] = prefix
#$ARGV[2] = input gene file.


open (INPUT, $ARGV[2]) or die "Cannot open $ARGV[2]";

while ($line = <INPUT>){
	$line =~ s/\s+$//;
	@split_line = split "\t", $line;
		push @array, $split_line[3];
		push @array, $split_line[4];
	@sort = sort {$a <=> $b} @array;
		$ID = $sort[0] . "_" . $sort[1];
		#print $ID, "\n";
	
	if ($split_line[5] =~ /^\.$/){
		next;
	}
	else{
		$Score_to_ID{$ID} = $split_line[5];
		#print $ID, "\t", $split_line[5], "\n";
	}
	undef (@array);
	undef (@sort);
}
close INPUT;

$output = $ARGV[1] . "\/" . "\/" . $ARGV[3] . "out\.gtf";
open (GTF, ">$output") or die "Cannot create $output";

open (GENE_TABLE, $ARGV[0]) or die "Cannot open $ARGV[0]";
while ($line = <GENE_TABLE>){
	$line =~ s/\s+$//;

	if ($line =~ /\#\#\#/){
		next;
	}
	if ($line =~ /./){
		@split_line = split "\t", $line;
		$start_stop = $split_line[2] . "_" . $split_line[3];

		$score = ".";
		if ($Score_to_ID{$start_stop} =~ /./){
			$score = $Score_to_ID{$start_stop};
		}
		 $split_line[0] =~ s/exon/CDS/;
		print GTF $split_line[6], "\t",
			"GSTATS", "\t",
			$split_line[0], "\t",
			$split_line[2], "\t",
			$split_line[3], "\t",
			$score, "\t",
			$split_line[4], "\t",
			".", "\t",
			$split_line[5], "\n";
	}
}		
close GENE_TABLE;
close GTF;
