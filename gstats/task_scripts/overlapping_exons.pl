#!usr/bin/perl
$overlapping_gene = 0;
$no_overlap = 0;
open (GENE_TABLE, $ARGV[0]) or die "Cannot open the gene table!!!";
open (OUTPUT, $ARGV[1]) or die "Cannot find the output argument!!!";

$output_nonov = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "non_overlapping_gene_table.txt";
$output_ov = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "overlapping_gene_table.txt";

open (NON_OVERLAPPING_GENE_TABLE, ">$output_nonov") or die "Cannot create non_overlapping_gene_table.txt at $ARGV[1]";
open (OVERLAPPING, ">$output_ov") or die "Cannot create overlapping_gene_table.txt at $ARGV[1]";
print OVERLAPPING "###", "\n";
print NON_OVERLAPPING_GENE_TABLE "###", "\n";

@infile = <GENE_TABLE>;
$infile = join "", @infile;
@GENE_TABLE = split "###", $infile;
$overlap = 0;

	foreach $gene (@GENE_TABLE){
		if ($gene =~ /gene/){
			$gene =~ s/\s+$//g;

			@split_lines = split "\n", $gene;
			foreach $line (@split_lines){
				$line =~ s/\s+$//;

				@tab = split "\t", $line;
				if ($tab[0] =~ /exon/){				
					for ($a = $tab[2]; $a <= $tab[3]; $a++){
						$all_spaces++;
						if ($array{$a} =~ /.+/){
							$overlap++;
						}
						else{
							$array{$a} = "exon";
						}
					}
				}
			}
		if ($overlap > 0){
			 print OVERLAPPING $gene, "\n", "###";		
			$overlapping_gene++;
		}
		if ($overlap == 0){
			print NON_OVERLAPPING_GENE_TABLE $gene, "\n", "###";
			$no_overlap++;
		}
	$overlap = 0;
	$all_spaces = 0;
	undef(%array);
	}
}
print NON_OVERLAPPING_GENE_TABLE "\n###";
print OVERLAPPING "\n###";
close NON_OVERLAPPING_GENE_TABLE;
close OVERLAPPING;

print "Number of genes with overlapping exons:\t", $overlapping_gene, "\n";
print "Number of genes without overlapping exons:\t", $no_overlap, "\n";

