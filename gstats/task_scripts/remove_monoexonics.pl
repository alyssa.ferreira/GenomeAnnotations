#!usr/bin/perl
# Removes monoexonics. Creates output multiexonic_genes.txt that is then reverted to gene_table.txt

open (GENE_TABLE, $ARGV[0]) or die "Cannot open the gene table!!!";
# ARGV[1] == output
# ARGV[0] == gene table
# ARGV[2] == prefix
      
	@infile = <GENE_TABLE>;
        $infile = join "", @infile;
        @infile = split "###", $infile;
close GENE_TABLE;

$multis = $ARGV[1] . "\/" . "\/" .  $ARGV[2] . "multiexonic_genes.txt";

open (OUTPUT, ">$multis") or die "Cannot create multiexonic_genes.txt at $ARGV[1]";
print OUTPUT "###", "\n";
	GENE: foreach $gene (@infile){
                $gene =~ s/\s+$//g;
                @split_lines  = split "\n", $gene;

		 foreach $line (@split_lines){
                        @split_tab = split "\t", $line;

                        if ($split_tab[0] =~ /intron/){
				print OUTPUT $gene, "\n", "###";
				$count++;
				next GENE;
			}
		}
	}
print "Number of genes retained:", "\t", $count, "\n";
close OUTPUT;
