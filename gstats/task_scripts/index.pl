#!usr/bin/perl
use Bio::Index::Fasta;
use Bio::Seq;

print "Creating index file from $ARGV[0]", "\n";
$file_name = $ARGV[0] . ".idx";

$inx = Bio::Index::Fasta->new(-filename => $file_name, -write_flag => 1);
$inx->make_index($ARGV[0]);



