#!usr/bin/perl
# ARGV[0] --> in fasta
# ARGV[1] --> output location
# ARGV[2] --> prefix

use Bio::Index::Fasta;

$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "genes_with_introns.fasta";
open (FASTA_1, ">$output") or die "Cannot create the output script!!!";

$input = $ARGV[0] . ".idx";
$inx = Bio::Index::Fasta->new(-filename => $input);

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";
open (GENE_TABLE, "$gene_table") or die "Cannot open $gene_table";
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;

GENE: foreach $gene (@gene_table){
	$gene =~ s/\s+$//g;
        @split_lines = split "\n", $gene;

        foreach $line (@split_lines){
        	$line =~ s/\s+$//;
                @split_tab = split "\t", $line;

                	if ($split_tab[0] =~ /gene/){
				$start = $split_tab[2];
				$end = $split_tab[3];
	
				$seq = $inx->fetch("$split_tab[6]");
				$string_1 = $seq->subseq("$start", "$end");
				print FASTA_1 "\>", $split_tab[5], "\n", $string_1, "\n";
			}
	}		
}

close FASTA_1;




