#!usr/bin/perl
$datestring = localtime();
######################################################################################################################################################################
#	GSTATS 
#	Madison.Caballero@uconn.edu
#	
#
#	About this script:
#		This script is designed to provide statistics based off annotation outputs such as .gft, .gff3, .gff2, and .gff3 files. To do this, the input 
#		script is evaluated and provided the proper supporting script to create accurate statistics. 
#

$INPUT = join " ", @ARGV;
$infile = scalar @ARGV - 1;
$output = scalar @ARGV - 2;

if (scalar @ARGV == 0){
	print 
		"Gstats\.\pl\n",
		"Contact\:\tMadison.Caballero\@uconn\.edu\n\n",
	        "Version: 04/24/2018\tUpdated:05/10/2018\n",
		"-------------------------------------------------------------GSTATS MANUAL-------------------------------------------------------------\n\n",
		"   Welcome to gstats, the one stop destination for all your annotation analysis needs. Except, probably not. Anyway, the script gstats.pl\n",
		"is a task-assigning script that calls upon many different scripts to analyze your .gff3, .gff, or .gtf files by creating a median format\n",
		"called gene_table.txt. From here, statisitcs can be calculated and distribution tables created. Please note: Everything is still under\n",
		"construction and flags are still being built in. Contact me with format or flag requests!", "\n\n",

		"To run this script\:\n",
		"\tperl gstats\.pl \-f \<format\> \[options\] \-O \<output directory\> \<input\_file\>\n\n",

		"Mandatory inputs\:\n",
		"\t\-f \<format\>\t\t I need to know the format of your input. All gff/gtf/gff3 files are different so I need to call\n",
			"\t\t\t\t\tthe right script.\n",
		"\t\-O \<output directory\>\t I need to know where to put the output files since the script runs in a shared space. Please write\n",
			"\t\t\t\t\tthe output ending with a \/ as the SECOND TO LAST argument\n",
		"\t\<input\_file\>\t\t There needs to be an input .gff3, .gtf, or .gff as the LAST argument.\n",		

		"\nOutput files always created:\n",
		"\tgstats_log.txt : What's happening\n",
		"\tgene_table.txt : A very readable table with information from your input\n",

		"\n\nSupported formats\:\n\n",
	
		"\t\-f \[format\]\t",
		"Specifying a format\: A mandatory step to call upon the right script.\n",	
		"\t\tAvailable formats\:\n",
	"\t\tGMAP:\n",
		"\t\t\tgmap_2017_03_17_gff3\n",						# gmap version 2017/03/17 gff3 for braker					
	"\t\tBRAKER:\n",
		"\t\t\tbraker_2.05_gtf\n",						# augustus version 3.2.3 gft for braker	
		"\t\t\tbraker_2.05_gff\n",	 					# augustus version 3.2.3 gff for braker
		"\t\t\tbraker_2.05_gff3\n",                                             # augustus version 3.2.3 gff3 for braker
		"\t\t\tbraker_2.0_gff3\n",						# braker version 2.0 gff3
		"\t\t\tbraker_2.0_gff\n",						# braker_2.0_gff
		"\t\t\tbraker_2.0_gtf\n",						# braker_2.0_gtf
		#"\t\t\taugustus_unknown_gtf\n",					# augustus_unknown_gtf
	"\t\tMAKER:\n",
		"\t\t\tmaker_2.31.9_gff\n",						# maker 2.31.9 gff
	"\t\tGENOME THREADER:\n",
		"\t\t\tgenomethreader_1.6.6_gff3\n",					# genomethreader_1.6.6_gff3
	"\t\tGFFREAD:\n",
		"\t\t\tgffread_0.9.12_gff3\n",						# gffread_0.9.12_gff3
	"\t\tEXONERATE:\n",
		"\t\t\texonerate_2.4.0_gff\n",						#exonerate_2.4.0_gff
	"\t\tGSTATS:\n",
		"\t\t\tgstats_gene_table\n",
		"\t\t\tgstats_gtf\n",

		"\n\nAdditional parameters you can include\:\n\n",

######FLAGS######

		"\t-p [prefix]\t\tAdds a prefix to every file created.",

		"\n\n",

		"\t\--statistics\t\tCreates the file statistics.txt file full of all sorts of relevant information. Statistics will always be\n",
		"\t\t\t\tperformed on the final gene set as directed by the flags and filters chosen.",

		"\n\n",

		"\t--statistics-at-every-step\tPerforms and prints statistics after every filtering step into the log. For the final statistics.txt file,\n",
		"\t\t\t\t\tbe sure to also include --statisitcs in your command.",

		"\n\n",

		"\t\-\-splice-rescue\t\tGenes with overlapping transcripts that are splice variants or incompletes will be retrieved and added\n",
		"\t\t\t\tinto gene_table.txt modified with a .version.",

		"\n\n",

		"\t--rem-start-introns\tGenes that begin (5' end) with an intron are removed. Remaining genes are kept in gene_table.txt",

		"\n\n",

		"\t--rem-end-introns\tGenes that end (3' end) with an intron are removed. Remaining genes are kept in gene_table.txt",

		"\n\n",

		"\t\-\-rem\-monoexonics\tMonoexonic genes are removed from gene_table.txt. Monoexonic genes are then not reflected in statistics,\n",
		"\t\t\t\tdistributions, or any fasta commands.",

		"\n\n",
	
		"\t--min-exon-size [number]\tRemoves genes that have exons smaller than a provided size. Default is 20. Remaining genes are kept in gene_table.txt",

		"\n\n",

		"\t--min-intron-size [number]\tRemoves genes that have introns smaller than a provided size. Default is 20. Remaining genes are kept in gene_table.txt",

                "\n\n",

		#"\t\--distributions <option> <option> ...\t",
                #"\n\t\tDistributions\: You may specify specific dsitributions you would like created or all. This can be a lengthy process\.\n",
		#"\t\tAvailable distributions:\n",
		
		#"\t\t\tmultiexonic_exon_lengths\t\tOutput file: distribution_multiexonic_exon_lengths.txt\n",		# Exon lengths 
		#"\t\t\tmultiexonic_intron_lengths\t\tOutput file: distribution_multiexonic_intron_lengths.txt\n", 	# Intron lengths
		#"\t\t\tmulti_exonic_gene_lengths\t\tOutput file: distribution_multiexonic_gene_lengths.txt\n",		# Gene lengths
		#"\t\t\texon_position_to_average_size\t\tOutput file: distribution_multiexonic_gene_lengths.txt\n",	# Exon position to average size
		#"\t\t\tintron_position_to_average_size\t\tOutput file: distribution_multiexonic_intron_lengths.txt\n",    # Exon position to average size


		#"\n\n",

		"Flags that require that you input a fasta file because seqeunce is being involved:",
		"\n\*\*\*NOTE: DO NOT RUN FASTA FLAGS WITHOUT --splice-rescue. Genetic coordinates may disrupt bioperl commands!\*\*\*",
		
		"\n\n",

		"\t--fasta \[/path/to/your/nucleotide/fasta.fasta\]\t\tThis must be the same fasta your gff3/gff/gtf is based on otherwise flags will fail!\n",
		"\t\t\t\t\t\t\t\tThe index will be created where the fasta is and have the postfix .idx.",		

		"\n\n",

		"\tFlags you can include once fasta is specified:\n\n",

		"\t\t--splice-table\t\tIntron splice types are surveyed and printed to a splice:count table called splice_table.txt.",		

		"\n\n",
		
		"\t\t--canonical-only\tGenes without canonical (gt-ag) splice sites are filtered out. Monoexonics will remain (to remove them,\n",
			"\t\t\t\t\tuse --rem-monoexonics).",
		
		"\n\n",
		
		"\t\t--rem-genes-without-start-codon\t\tGenes that have a start codon are kept in gene_table.txt. Those that do not are discarded.",

		"\n\n",

		"\t\t--rem-genes-without-stop-codon\t\tGenes that have a stop codon are kept in gene_table.txt. Those that do not are discarded.",

		"\n\n",

		"\t\t--get-fasta-with-introns\tNucleotide fasta creator, introns included. Makes the file genes_with_introns.fasta from the final filtered", "\n",
		"\t\t\t\t\t\tversion of gene_table.txt. Header names are the fifth column of gene_table.txt (typically the gene ID).",

		"\n\n",

                "\t\t--get-fasta-without-introns\tNucleotide fasta creator, introns excluded. Makes the file genes_without_introns.fasta from the final filtered", "\n",
                "\t\t\t\t\t\tversion of gene_table.txt. Header names are the fifth column of gene_table.txt (typically the gene ID).",
	
		"\n\n",

		"\t\t--create-gtf\tA \gtf file called out.gtf will be created with CDS, intron, gene, and start/stop codon information.\n";

		"\n\n";

		die "\n\n";
} 

###############################################################################################################################################################
# Prefix

if ($INPUT =~ /\s\-p\s+(.+?)\s+/){
	$prefix = $1;
	$pre = $prefix . "_";
}

$log = "$ARGV[$output]\/\/" . $pre . "gstats_log.txt";
open (LOG, ">$log");
close LOG;

open (LOG, ">>$log");

	print LOG "Time of run: ", $datestring, "\n";
	print LOG "Command: ", $INPUT, "\n\n"; 

	if ($prefix =~ /./){
		print LOG "You have specified a prefix\:\t", $prefix, "\n\n";
	}
if ($INPUT =~ /\-f\s*(.+)\s*/){
	print LOG "You have specfied a format\:\t";						# Format specifications

	#FORMAT DECISIONS#########################################################################################################
	
	if ($1 =~ /gmap_2017_03_17_gff3/){						# gmap_2017_03_17_gff3
		print LOG "GMAP version 2017\/03\/17 in gff3 format\n";
		$format = "gmap_2017_03_17_gff3";		
	}
	if ($1 =~ /braker\_2\.05\_gtf/){                                             	# braker_2.05_gtf
                print LOG "BRAKER version 2\.05 in gtf format\n";
		$format = "braker\_2\.05\_gtf";		
        }
	if ($1 =~ /braker\_2\.05\_gff\s/){                                             	# braker_2.05_gff
                $format = "braker\_2\.05\_gff";		
    		print LOG "BRAKER version 2\.05 in gff format\n";
	}
	if ($1 =~ /braker\_2\.05\_gff3/){                                  		# braker_2.05_gff3
               	  print LOG "BRAKER version 2\.05 in gff3 format\n";
        	  $format = "braker\_2\.05\_gff3";
	}
	if ($1 =~ /braker\_2\.0\_gff3/){						# braker_2.0_gff3
		print LOG "BRAKER version 2\.0 in gff3 format\n";
                $format = "braker\_2\.0\_gff3";
	}
	if ($1 =~ /augustus\_unknown\_gtf/){						# augustus unknown gtf
		print LOG "AUGUSTUS version UNKNOWN in gtf format\n";
                $format = "augustus\_unknown\_gtf";
	}
	if ($1 =~ /genomethreader\_1\.6\.6\_gff3/){					# genomethreader_1.6.6_gff3
		print LOG "GENOMETHREADER version 1.6.6 in gff3 format\n";
		$format = "genomethreader\_1\.6\.6\_gff3";
	}
	if($1 =~ /gffread\_0\.9\.12\_gff3/){						# gffread_0.9.12_gff3
		print LOG "GFFREAD version 0.9.12 in gff3 format\n";
		$format = "gffread\_0\.9\.12\_gff3";	
	}
	if ($1 =~ /braker\_2\.0\_gff\s/){
		print LOG "BRAKER version 2\.0 in gff format\n";			# braker_2.0_gff
		$format = "braker_2\.0_gff";
	}
	if ($1 =~ /braker\_2\.0\_gtf\s/){						# braker_2.0_gtf
		print LOG "BRAKER version 2\.0 in gtf format\n";		
		$format = "braker\_2\.0\_gff";
	}
	if ($1 =~ /exonerate\_2\.4\.0\_gff/){						#exonerate_2.4.0_gff
		print LOG "EXONERATE version 2\.4\.0 in gff format\n";
		$format = "exonerate\_2\.4\.0\_gff";
	}
	if ($1 =~ /maker\_2\.31\.9\_gff/){
		print LOG "MAKER version 2.31.9 in gff format\n";
		$format = "maker_2.31.9_gff";
	}
	if ($1 =~ /gstats_gene_table/){
		print LOG "GSTATS all version in gene_table.txt format\n";
		$format = "gstats_gene_table";
	}
	if ($1 =~ /gstats_gtf/){
		print LOG "GSTATS all version in out.gtf format\n";
		$format = "gstats_gtf";
	}
	#else{
	#	print LOG "You have not specified a valid format.";
	#        die "You must specify a valid format!";
	#}
	##########################################################################################################################
}
else {											# Format not specified
	print LOG "You have not specified a format.";
	die "You must specify a format!";
}

if ($INPUT =~ /\-O\s*$ARGV[$output]/){
	if ($ARGV[$output] =~ /.+$/){ print LOG "You have specified an output directory: $ARGV[$output]\n";}
	#else{ die "Please end your output directory with a \/";}
}
else{ die "You must specify an output directory using the -O command. It must also be before your input file.\n";} 

#LOG OF FORMAT###################################################################################################################################################
#COMMAND#########################################################################################################################################################
#CONVERSION TO GENE TABLE

$infile = scalar @ARGV - 1;
$gene_table = $ARGV[$output] . "\/" . "\/" . $pre . "gene_table.txt";

push @command, "perl ";							# MANDATORY
push @command, "format_scripts\/";					# MANDATORY --> LOCATION OF FORMAT SCRIPTS
push @command, $format;							# MANDATORY --> FORMAT
push @command, "\.pl ";							# MANDATORY
push @command, "$ARGV[$infile] ";					# MANDATORY --> INFILE
push @command, "$ARGV[$output] ";					# MANDATORY --> outdir
push @command, "$pre ";							# MANDATORY --> prefix
#push @command, "> ";
#push @command, $gene_table;

$command = join "", @command;
system "$command";

print LOG "\n", "Format command:\t", $command, "\n";			# PRINT TO LOG THE COMMAND

## THE OUTPUT gene_table.txt HAS BEEN CREATED AT THIS POINT at the specified location!

###############################################################################################################################################################
## FLAG TO SCRIPT STATS

print LOG "\n\nFlags:\n\n";

if ($INPUT =~ /\-\-splice-rescue/){
	print LOG "--splice-resuce has been activated. I will look for overlapping gene space and see if it is the result of multiple transcripts mapping to the same location. Splice variants will be labeled as [gene].1, [gene].2, etc... and added to the end of gene_table.txt.";

	$string = "perl task_scripts/overlapping_exons.pl $gene_table $ARGV[$output] $pre >> $log";
        print LOG "\n\tCommand: ", "$string", "\n";
	print LOG "Results:\n";

	system ($string);
	$name = $ARGV[$output] . "\/" . "\/" . $pre . "non_overlapping_gene_table.txt";
	$string = "mv $name $gene_table";
	system ($string);

	$string = "perl task_scripts/splice_variants.pl $gene_table $ARGV[$output] $pre >> $log";

	print LOG "\tCommand: ", $string, "\n";
        system ($string);
	
	$name = $ARGV[$output] . "\/" . "\/" . $pre . "transcripts.txt";
        system "cat $name >> $gene_table";
	system "rm $name";
	#$name = $ARGV[$output] . "\/" . "\/" . $pre . "non_overlapping_gene_table.txt";
	#system "rm $name";
	$name = $ARGV[$output] . "\/" . "\/" . $pre . "overlapping_gene_table.txt";
	system "rm $name";
	
	if ($INPUT =~ /\-\-statistics-at-every-step/){
	        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
	        $string =  "perl task_scripts/classic_stats.pl $gene_table >> $log";
		print LOG "\n";
	        system ($string);
	}


}

#############################################################################################################################################################
if ($INPUT =~ /--rem-start-introns/){
	print LOG "\n--rem-start-introns has been activated. Genes that start with intronic space are removed. Those that do not are kept in gene_table.txt.\n";
	$string = "perl task_scripts/remove_starting_introns.pl $gene_table $ARGV[$output] $pre >> $log";
	
	print LOG "\tCommand: ", $string, "\n";
	print LOG "Results:", "\n";
	system ($string);

	$name = $ARGV[$output] . "\/" . "\/" . $pre . "complete_genes.txt";
	system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
}
if ($INPUT =~ /--rem-end-introns/){
        print LOG "\n--rem-end-introns has been activated. Genes that end with intronic space are removed. Those that do not are kept in gene_table.txt.\n";
        $string = "perl task_scripts/remove_ending_introns.pl $gene_table $ARGV[$output] $pre >> $log";

        print LOG "\tCommand: ", $string, "\n";
        print LOG "Results:", "\n";
        system ($string);

	$name = $ARGV[$output] . "\/" . "\/" . $pre . "complete_genes.txt";
        system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
}
############################################################################################################################################################
if ($INPUT =~ /\-\-rem\-monoexonics/){
	print LOG "\n--rem-monoexonics has been activated. Genes that don't have an intron (2+ exons) will be removed from the gene_table.txt.\n";
	$string = "perl task_scripts/remove_monoexonics.pl $gene_table $ARGV[$output] $pre >> $log";
	print LOG "\tCommand: ", $string, "\n";
	print LOG "Results:", "\n";	

	system ($string);

	$multi = $ARGV[$output] . "\/" . "\/" . $pre . "multiexonic_genes.txt";
	system "mv $multi $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
}

#############################################################################################################################################################
if ($INPUT =~ /\-\-min-exon-size/){
	if ($INPUT =~ /--min-exon-size\s+(\d+?)\s/){$min = $1;}
	else{$min = 20;}

	print LOG "\n--min-exon-size has been activated. Genes with an exon that is less than $min will be removed from gene_table.txt";
	$string = "perl task_scripts/minimum_exon.pl $gene_table $ARGV[$output] $min  $pre >> $log";
	print LOG "\n\tCommand: ", $string, "\n";
	print LOG "Results:\n";
	system ($string);

	$name = $ARGV[$output] . "\/" . "\/" . $pre . "minumum_exons_gene_table.txt";
	system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
}
if ($INPUT =~ /\-\-min-intron-size/){
        if ($INPUT =~ /--min-intron-size\s+(\d+?)\s/){$min = $1;}
        else{$min = 20;}

        print LOG "\n--min-intron-size has been activated. Genes with an intron that is less than $min will be removed from gene_table.txt";
        $string = "perl task_scripts/minimum_intron.pl $gene_table $ARGV[$output] $min $pre >> $log";
        print LOG "\n\tCommand: ", $string, "\n";
        print LOG "Results:\n";
        system ($string);

        $name = $ARGV[$output] . "\/" . "\/" . $pre . "minumum_introns_gene_table.txt";
        system "mv $name $gene_table";

	if ($INPUT =~ /\-\-statistics-at-every-step/){
                $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                $string =  "perl task_scripts/classic_stats.pl $gene_table >> $log";
                print LOG "\n";
                system ($string);
        }
}


#############################################################################################################################################################

#############################################################################################################################################################
# FASTA FANCY
#
	use Bio::Index::Fasta;
        use Bio::Seq;

if ($INPUT =~ /--fasta\s*.+\.fasta\s/ or $INPUT =~ /--fasta\s*.+\.fa\s/){
	print LOG "\nYou have specified a fasta! Indexing it...\n";							# INDEXING FASTA
		for ($a = 0; $a < scalar @ARGV; $a++){if ($ARGV[$a] =~ /--fasta/){$fasta = $ARGV[$a+1];}}
		$file_name = $fasta . ".idx";										# Creating Index file name

		$string = "perl task_scripts/index.pl $fasta >> $log";
		system ($string);	

	if ($INPUT =~ /\-\-canonical-only\s/){
		print LOG "\n--canonical-only has been activated. Genes without canonical gt-ag splice sites are filterd out. Monoexonics remain if not removed with flag.\n";
		$string = "perl task_scripts/canonical_only.pl $fasta $ARGV[$output] $pre >> $log";
		print LOG "\tCommand: ", $string, "\n";
		print LOG "Results:\n";
		system ($string);

		$canonical = $ARGV[$output] . "\/" . "\/" . $pre . "canonical_genes.txt";
		system "mv $canonical $gene_table";

		if ($INPUT =~ /\-\-statistics-at-every-step/){
                	$statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
        	        $string =  "perl task_scripts/classic_stats.pl $gene_table >> $log";
	                print LOG "\n";
                	system ($string);
        	}
	}
	if ($INPUT =~ /--rem-genes-without-start-codon/){
		print LOG "\n--rem-genes-without-start-codon has been activated. Genes that have a start codon are kept in gene_table.txt.\n";
		$string = "perl task_scripts/rem_genes_without_start.pl $fasta $ARGV[$output] $pre >> $log";
		print LOG "\tCommand: ", $string, "\n";
		print LOG "Results:\n";
		system ($string);
		$name = $ARGV[$output] . "\/" . "\/" . $pre . "start_and_stop_gene_table.txt";
		$string = "mv $name $gene_table";
		system ($string);

		if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl task_scripts/classic_stats.pl $gene_table >> $log";
                        print LOG "\n";
                        system ($string);
                }

	}
	if ($INPUT =~ /--rem-genes-without-stop-codon/){
                print LOG "\n--rem-genes-without-stop-codon has been activated. Genes that have a stop codon are kept in gene_table.txt.\n";
                $string = "perl task_scripts/rem_genes_without_end.pl $fasta $ARGV[$output] $pre >> $log";
                print LOG "\tCommand: ", $string, "\n";
                print LOG "Results:\n";
                system ($string);
                $name = $ARGV[$output] . "\/" . "\/" . $pre . "start_and_stop_gene_table.txt";
                $string = "mv $name $gene_table";
                system ($string);

		if ($INPUT =~ /\-\-statistics-at-every-step/){
                        $statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
                        $string =  "perl task_scripts/classic_stats.pl $gene_table >> $log";
                        print LOG "\n";
                        system ($string);
                }
        }
	if ($INPUT =~ /\-\-splice\-table\s/){
                print LOG "\n--splice-table has been activated. Creating splice_table.txt catalog.";
                $string = "perl task_scripts/splice_table.pl $fasta $ARGV[$output] $pre";
                print LOG "\n\tCommand: ", "$string", "\n";
                system ($string);
        }
	if($INPUT =~ /--get-fasta-with-introns/){
		print LOG "\n--get-fasta-with-introns has been activated. Nucleotide fasta will be created called genes_with_introns.fasta.";
		$string = "perl task_scripts/get_fasta_with_introns.pl $fasta $ARGV[$output] $pre";
		print LOG "\n\tCommand: ", $string, "\n";
		system ($string);
	}
	if ($INPUT =~ /--get-fasta-without-introns/){
                print LOG "\n--get-fasta-without-introns has been activated. Nucleotide fasta will be created called genes_without_introns.fasta.";
                $string = "perl task_scripts/get_fasta_without_introns.pl $fasta $ARGV[$output] $pre";
                print LOG "\n\tCommand: ", $string, "\n";
                system ($string);
        }
	if ($INPUT =~ /\-\-create\-gtf/){
		print LOG "\n--create-gtf has been activated. A gtf file called out.gtf will be created.";
		$string = "perl task_scripts/add_start_stop_to_gene_table.pl $fasta $ARGV[$output] $pre";
		print LOG "\n\tCommand: ", $string, "\n";
		system ($string);

		$start_stop_table = $ARGV[$output] . "\/" . "\/" . $pre . "start_and_stop_gene_table.txt";

		$string = "perl task_scripts/gtf_creator.pl $start_stop_table $ARGV[$output] $ARGV[$infile] $pre";
		print LOG "\tCommand: ", $string, "\n";
		system ($string);

		system "rm $start_stop_table";
	}
}

###########################################################################################################################################################

if ($INPUT =~ /\-\-statistics\s/){

	$statistics = $ARGV[$output] . "\/" . "\/" . $pre . "statistics.txt";
        $string =  "perl task_scripts/classic_stats.pl $gene_table > $statistics";
        system ($string);

        print LOG  "\n--statistics has been activated. Statistics will be printed to statistics.txt\n";
        print LOG "\tCommand: ", $string, "\n";

}

############################################################################################################################################################

if ($INPUT =~ /\-\-distributions/){
        print LOG "\n--distributions have been activated! You have chosen:\n";

        if ($INPUT =~ /multiexonic_exon_lengths/){
                print LOG "\tmultiexonic_exon_lengths\n";
                $string = "perl task_scripts/distributions/multi_exonic_exon_lengths.pl $gene_table $ARGV[$output]";
                system ($string);
        }
        if ($INPUT =~ /multiexonic_intron_lengths/){
                print LOG "\tmultiexonic_intron_lengths\n";
                $string = "perl task_scripts/distributions/multi_exonic_intron_lengths.pl $gene_table $ARGV[$output]";
                system ($string);
        }
        if ($INPUT =~ /multi_exonic_gene_lengths/){
                print LOG "\tmultiexonic_gene_lengths\n";
                $string = "perl task_scripts/distributions/multi_exonic_gene_lengths.pl $gene_table $ARGV[$output]";
                system ($string);
        }
        if ($INPUT =~ /exon_position_to_average_size/){
                print LOG "\texon_position_to_average_size\n";
                $string = "perl task_scripts/distributions/exon_position_to_average_size.pl $gene_table $ARGV[$output]";
                system ($string);
        }
        if ($INPUT =~ /intron_position_to_average_size/){
                print LOG "\tintron_position_to_average_size\n";
                $string = "perl task_scripts/distributions/intron_position_to_average_size.pl $gene_table $ARGV[$output]";
                system ($string);
        }
}
###########################################################################################################################################################

print LOG "\nCompleted! Have a great day!\n\n";

$temp = $ARGV[$output] . "\/" . "\/" . $pre . "temp_file.txt";
system "rm $temp";

close LOG;
