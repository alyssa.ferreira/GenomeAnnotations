#!/bin/bash
#SBATCH --job-name=gstats
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=Madison.Caballero@uconn.edu
#SBATCH -o stats_%j.o
#SBATCH -e stats_%j.e


module load perl

perl gstats.pl \
-f gffread_0.9.12_gff3 \
--statistics \
--statistics-at-every-step \
-p test \
--splice-rescue \
--rem-start-introns \
--rem-monoexonics \
--rem-end-introns \
--canonical-only \
--splice-table \
--min-exon-size 40 \
--min-intron-size 40 \
--rem-genes-without-start-codon \
--rem-genes-without-stop-codon \
--get-fasta-without-introns \
--get-fasta-with-introns \
--create-gtf \ 
--fasta /home/FCAM/mcaballero/gstats/task_scripts/pita.v2.0.1.masked.3k.fasta \
-O /home/FCAM/mcaballero/gstats/test_output/ \
/home/FCAM/mcaballero/gstats/test_formats/gffread_0.9.12.gff3
