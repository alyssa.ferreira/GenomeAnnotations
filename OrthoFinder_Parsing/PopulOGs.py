##
## Plant Computational Genomics Laboratory
## University of Connecticut
##
## Contact - alexander.trouern-trend@uconn.edu
##
## PopulOGs.py 
##
##
## Returns a list of "popular" Orthogroups
## (those shared by all OrthoFinder input)
##
##
##


with open("Orthogroups.csv") as f:
    time = f.readlines()
## Determine the column number for each species
check = 0
for line in time:
    check += 1
    sline = line.split("\t")
    if check == 1:
        check2 = 0
        print "############# Column Designations ##############"
	for column in sline:
            print "Column " + str(check2) + " = " + sline[check2]
            check2 += 1


## Find only the lines that have content in each column.
## If a column is blank, the orthogroup is not composed of
## transcripts/genes from every input file
            
final_count = len(sline)
print "########## Shared Orthogroups Listed Below ###########"
for line in time:
    sline2 = line.split("\t")
    count = 0
    #print len(sline2)
    for x in range(len(sline2)):
        if sline2[x] != "":
            count += 1
            #print count
            if int(count) == int(final_count):
	        print sline2[0]			
	        #print line



