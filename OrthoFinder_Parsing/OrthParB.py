import operator

with open("OrthPar_LenTable") as f:
    lentable = f.readlines()

with open("orthogroups.csv") as g:
    groups = g.readlines()

#*****NOTE, using zero-based indexing
# OrthParB.py takes the OrthPar_LenTable produced by OrthParA.py and uses it to compare the sequence length (OrthPar_LenTable column 1) for each transcript within each orthogroup (Orthogroups.csv column 1)


for line in groups:   			# Accessing Orthogroup table which lists transcripts per orthogroup
    wanted = []
    line = line.strip()
    line_sep = line.split("\t")		 # Parsing
    OG = line_sep[0]
    for col in line_sep[1:]:     	# still parsing 
        genegetter = col.split(" ")
        for x in genegetter:
            wanted.append(x)
    longest = 0				# placeholder reference to determine longest transcript
    longest_name = ""
    pairs = {}				# A dictionary for storing Transcript:Length pairs. Resets for each orthogroup.
    for entry in lentable:		# Accessing OrthPar_LenTable produced using OrthParA.py
   	entry = entry.strip()
	entry_sep = entry.split("\t")	# Column Parsing
        header = entry_sep[0]
	length = entry_sep[1]
	for item in wanted:
            if item == header:
	        if int(length) >= int(longest):
		    longest = length
		    longest_name = header
    print OG + "\t" +  longest_name
